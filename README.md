# Signature Detection Server


## Getting Started

This is a web server used for Signature Detection and Validation for documents.
It receives a request specifing multiple places in a document  where a signature is supposed to be and check if a signature is present.

### Prerequisites

The recommended platform for use is Linux Ubuntu with Python 3.6.9.

Check the "requirements.txt" file for packages.
Use the following instructions for installing them:
```pip install -r requirements.txt```


### Installing 

1. Clone this repository.
2. Run this command in the repo directtory: ```python3 manage.py runserver```.
3. This will open port 8000 on the 127.0.0.1.

The server only supports a single POST method at ```/detection``` path.


### Usage


The server supports only POST requests at the ```/detection``` path.
The format for the request is the folowing:

``` documentContents: image_file```  
``` documentType: image_type```  
``` signatures:```
``` json
[
	{
		"anchorText": "Signature",
		"id":"id1",
			
				"xLeftCoord": 830,
				"yTopCoord": 1439,
				"xRightCoord": 1188,
				"yBottomCoord": 1499
			
		
	},
	{
		"anchorText": "Signature",
		"id":"id2",
			
				"xLeftCoord": 819,
				"yTopCoord": 1661,
				"xRightCoord": 1202,
				"yBottomCoord": 1712
			
		
	}
]
```  

The fields in the request have the following meanings:  
1. documentContents: the image of the scanned document.
2. documentType: format of the picture sent.
3. signatures: Json format strcture for the required positions of the signature. Each element in this list has the 
following structure.

    * anchorText: the text of the label assigned with a specific signature in the document.
    * id : text used to uniquely indentify the result of that specific location for the signature.
    * xLeftCoord: upper left 'x' coordinate
    * yTopCoord: upper left 'y' coordinate
    * xRightCoord: lower right 'x' coordinate
    * yBottomCoord: lower right 'y' coordinate

The response with a status of 200 has the following structure:  
``` json
{
    "signaturesFound": [
        {
            "id": "id1",
            "found": true,
            "imageType": "jpg",
            "image": "b'/base64string"
        },
        {
            "id": "id2",
            "found": true,
            "image": "b'/base64string",
            "imageType": "jpg"
        }
    ],
    "status": 200
}
```  
The fields in the response have the following meanings:  
1. signaturesFound: list with details about found signatures.
    The details are the following:
    * id: given id from the request
    * found:  boolean field. true if the signature is present, false otherwise.
    * imageType: the format of the image type.
    * image: image of the signature formatted in base64 format.

2. status: status of the HTTP request.

















