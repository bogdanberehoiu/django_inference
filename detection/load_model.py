from .models import *  # set ONNX_EXPORT in models.py

device = 'cpu'
weights = os.path.join(os.getcwd(),"detection/weights/best.pt")
print(os.getcwd())
model = Darknet(os.path.join(os.getcwd(),"detection/cfg/yolov3-1cls.cfg"), (320, 192))
model.load_state_dict(torch.load(weights, map_location=device)['model'])
model.to(device).eval()
