from django.shortcuts import render
from django.http import HttpResponse
from .forms import UploadFileForm
from PIL import Image
import io as StringIO
import cv2

from .detect import detect_simple
from .aws_utils import client, AWS_S3, put_s3_data, get_s3_data, image_to_s3

import numpy as np
import json
from django.http import JsonResponse
import ast
import math
import sys
import os

from PIL import Image

import base64
import io
import uuid

def get_ocr(img, format):
    """Retrieve the OCR result of an image using the OCR lambda """
    key_input = str(uuid.uuid4())
    key_output = str(uuid.uuid4())
    key_input = key_input+"."+format
    key_output = key_output+".json"
    context = json.dumps("{}").encode("utf-8")
    context = base64.b64encode(context).decode('utf-8')
    
    image_to_s3(img, AWS_S3["input"],key_input, format)
    package = "{\"S3SourceBucket\":\""+AWS_S3["input"]+"\",\"S3SourceKey\":\""+key_input+"\",\
        \"S3DestinationBucket\":\""+AWS_S3["output"]+"\",\"S3DestinationKey\":\""+key_output+"\"}"
    payload = {"Records":[{"body":package}]}
    response = client.invoke(FunctionName="GoogleOCRService", InvocationType='Event', \
        Payload=json.dumps(payload), ClientContext=context)
    data = False
    while not data:
        try:
            data = get_s3_data(AWS_S3["output"], key_output)
        except Exception as e:
            pass
    ocr_json = json.loads(data)

    return ocr_json


def handle_image(f, coordinates):
    """preprocessing of the image and detecting signatures"""
    data = b''
    response = {}
    for chunk in f.chunks():
        data += chunk

    stream = StringIO.BytesIO(data)
    imgF = Image.open(stream).convert("RGB")
    imcv = cv2.cvtColor(np.array(imgF), cv2.COLOR_RGB2BGR)
    ret, imcv = cv2.threshold(imcv,200,255,cv2.THRESH_BINARY)
    coordinates,img = detect_simple(imcv, coordinates)
    return coordinates, img, imgF 

def get_anchor_coordinates(doc, anchor):
    """return positions of tha anchor in the document using the OCR result"""
    found = False
    positions = []
    for p in doc["fullTextAnnotation"]["pages"]:
        for b in p["blocks"]:
            for paragraph in b["paragraphs"]:
                words = []
                for word in paragraph["words"]:
                    words.append(''.join([s['text'] for s in  word["symbols"]]))
                words = " ".join(words)
                if words == anchor:
                    position = paragraph["boundingBox"]["vertices"]
                    positions.append(position)
                    found = True
    if found:
        return positions
    else:
        return False


def do_overlap(sig1, sig2, box1, box2): 
    """function that detects if two rectangles overlap"""
    boxA = [sig1[0],sig1[1],sig2[0],sig2[1]]
    boxB = [box1[0],box1[1],box2[0],box2[1]]
    xA = max(boxA[0], boxB[0])
    yA = max(boxA[1], boxB[1])
    xB = min(boxA[2], boxB[2])
    yB = min(boxA[3], boxB[3])

    # compute the area of intersection rectangle
    interArea = abs(max((xB - xA, 0)) * max((yB - yA), 0))
    if interArea == 0:
        return False

    return True
    

def write_results(img, l1, l2, r1, r2):
    """draw rectangels in a image and store it on disk"""
    img = np.array(img)

    img = cv2.rectangle(img, r1,r2, (0,255,0), 3)
    img = cv2.rectangle(img, l1,l2, (255,255,0), 3)

    cv2.imwrite("boxes.png", img)

def convert(anchor):
    """convert request form anchors to a more simple form """
    new_anchor = []
    new_anchor.append(anchor["anchorText"])
    new_anchor.append(anchor["id"])
    c0 = [anchor['xLeftCoord'],anchor['yTopCoord']]
    c1 = [anchor['xRightCoord'],anchor['yBottomCoord']]
    new_anchor.append([c0, c1])
    return new_anchor


# Create your views here.
def index(request):
    """main function for response"""
    if request.method == "POST":
        form = UploadFileForm(request.POST,request.FILES)
        if form.is_valid():
            try:
                anchors = json.loads(request.POST["signatures"])
            except Exception as e:
                return JsonResponse([{"body":"Invalid anchors data!"}, {"status":400}])
            response = {}
            anchors = list(map(convert, anchors))
            coordinates, img, imgF = handle_image(request.FILES['documentContents'], [anchor[2] for anchor in anchors])
            coordinates_ocr = {}
            responses = {}
            responses["signaturesFound"] = []

            file_extension = request.POST["documentType"]
            ocr_json = get_ocr(imgF, file_extension)

            found = []
            for anchor in anchors:
                box_s = anchor[2]
                name = anchor[1]
                response = {} 
                response['id'] = name
                coordinates_ocr[name] = get_anchor_coordinates(ocr_json, anchor[0])
                if coordinates_ocr[anchor[1]] == False:
                    response["found"] = False
                    responses["signaturesFound"].append(response)
                    continue
                match = False
                for a in coordinates_ocr[name]:

                    a = (a[0]['x'], a[0]['y'])

                    if a not in found:

                        box = {}
                        box[0] = tuple([box_s[0][0] + a[0], box_s[0][1] + a[1]])
                        box[1] = tuple([box_s[1][0] + a[0], box_s[1][1] + a[1]])

                        for sign in coordinates:
                           
                            img = cv2.rectangle(img, box[0], box[1], (0,0,255), 2)

                            if do_overlap(sign[0],sign[1],box[0],box[1]):

                                add_h = int((sign[1][1]-sign[0][1]) * 0.1)
                                add_w = int((sign[1][0]-sign[0][0]) * 0.1)
                                sig_img = img[sign[0][1]:sign[0][1] + add_h + sign[1][1]-sign[0][1],sign[0][0]:sign[0][0] + add_w + sign[1][0]-sign[0][0]]
                                retval, buffer = cv2.imencode('.jpg', sig_img)
                                jpg_as_text = base64.b64encode(buffer)

                                response["found"] = True
                                response["imageType"] = "jpg"
                                response["image"] = str(jpg_as_text)
                                found.append(a)
                                img = cv2.rectangle(img, sign[0], sign[1], (0,255,0), 2) 
                                img = cv2.rectangle(img, box[0], box[1], (255,0,0), 2) 
                                match = True
                                break

                        if match:
                            responses["signaturesFound"].append(response)
                            break
                        else:
                            response["found"] = False
                            responses["signaturesFound"].append(response)
                            break
        
            responses["status"] = 200
            cv2.imwrite("test.png", img)
            return JsonResponse(responses)
        else:
            return JsonResponse([{"body":"Invalid files sent!"}, {"status":400}, {"files":str(request.FILES)}], safe=False)
    return JsonResponse([{"body":"Only POST method allowed!"}, {"status":403}], safe=False)