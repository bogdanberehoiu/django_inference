from django import forms

class UploadFileForm(forms.Form):
    documentContents = forms.ImageField()
    documentType = forms.TextInput()
    signatures = forms.TextInput()