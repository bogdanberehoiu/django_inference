import boto3
from io import BytesIO

client = boto3.client('lambda')
S3 = boto3.resource("s3")
s2_client = boto3.client('s3')


AWS_S3 = {"input":"documents-manager-input-images",
            "output":"documents-manager-output-ocr"}

def put_s3_data(data, bucket, key):
    """function that puts data in a S3 object"""
    obj = S3.Object(bucket, key)
    return obj.put(Body=data)

def image_to_s3(img, bucket, key, format):
    """PIL image to S3"""
    buffer = BytesIO()
    format = format.upper()
    img.save(buffer, format=format)
    buffer.seek(0)
    sent_data = s2_client.put_object(Bucket=bucket, Key=key, Body=buffer)

def get_s3_data(bucket: str, key: str):
    """retrieves data from S3"""
    obj = S3.Object(bucket, key)
    return obj.get()['Body'].read().decode('utf-8')